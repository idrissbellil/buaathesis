thesis:
	xelatex thesis.tex
	-bibtex  thesis
	xelatex thesis.tex
	makeglossaries thesis
	xelatex thesis.tex
clean:
	find . -name '*.aux' -print0 | xargs -0 rm -rf
	rm -rf *.lof *.log *.lot *.out *.toc *.bbl *.blg *.thm
depclean: clean
	rm -rf *.pdf
